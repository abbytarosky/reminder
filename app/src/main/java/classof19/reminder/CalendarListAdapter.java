package classof19.reminder;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Samuel on 4/4/2017.
 */

public class CalendarListAdapter extends ArrayAdapter<Request> {

    public CalendarListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public CalendarListAdapter(Context context, List<Request> items) {
        super(context,R.layout.calendar_listview, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.calendar_listview, null);
        }

        Request p = getItem(position);


        TextView name = (TextView) v.findViewById(R.id.name);
        TextView description = (TextView) v.findViewById(R.id.description);

        if(p.getComplete() ==1)
        {
            name.setPaintFlags(name.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            name.setTextColor(getContext().getResources().getColor(R.color.colorAccentLight));
        }

        name.setText(p.getName());
        description.setText(p.getDescription());







        return v;
    }

}