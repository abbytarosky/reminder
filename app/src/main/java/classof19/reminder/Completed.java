package classof19.reminder;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

/**
 * Created by Samuel on 4/9/2017.
 */

public class Completed extends Fragment{
    View currentView;
    int oldnum;

    Spinner spinner;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        currentView = inflater.inflate(R.layout.completed_layout, container, false);
        Button submit = (Button) currentView.findViewById(R.id.submit_info);
        submit.setOnClickListener(submit());
        spinner= (Spinner) currentView.findViewById(R.id.categories);

        // defines the 3 categories - np
        ArrayList<String> categories = new ArrayList<String>();
        categories.add("Strength");
        categories.add("Intellect");
        categories.add("Discipline");

        // adapter set to category spinner - np
        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(categoryAdapter);
        // sets the click listener - np
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cat = parent.getItemAtPosition(position).toString();         // sets whichever one clicked to cat
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                cat = "Strength";
            }
        });
        return currentView;
    }

    String cat = "Strength";
    public View.OnClickListener submit() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                EditText name_input = (EditText) currentView.findViewById(R.id.nameInput);
                String name = name_input.getText().toString();

                EditText date_input = (EditText) currentView.findViewById(R.id.dateInput);
                String date = date_input.getText().toString();

                // reads input from description - np
                EditText description_input = (EditText) currentView.findViewById(R.id.descriptionInput);
                String description = description_input.getText().toString();

                SQL_Save_Request db = new SQL_Save_Request(getContext());


                // Creates a new request by calling Request - np
                Request newRequest = new Request(name, date, description, cat);
                db.addRequest(newRequest);
                replaceFragment(new Home());
*/
            }

        };
    }
    public void replaceFragment(Fragment fragment) {

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        getFragmentManager().popBackStack();

        fragmentTransaction.commit();
    }
}
