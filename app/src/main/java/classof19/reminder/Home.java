package classof19.reminder;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Abby on 3/19/2017.
 */

public class Home extends Fragment {
    View currentView;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        currentView = inflater.inflate(R.layout.home_activity, container, false);

        //TIM'S CODE
        // the magical saved data that is who knows where
        SharedPreferences values = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String Strength = values.getString("Strength","500");
        String Intelect = values.getString("Intelect","2100");
        String Discipline = values.getString("Discipline","333");
        //add the other values here

        //level math
        int strLevel = 1 + Integer.parseInt(Strength)/1000;   //governs the level of the stat
        int intLevel = 1 + Integer.parseInt(Intelect)/1000;
        int disLevel = 1 + Integer.parseInt(Discipline)/1000;
        int strMod = (Integer.parseInt(Strength)%1000)/10;  // governs the amount the bar is filled
        int intMod = (Integer.parseInt(Intelect)%1000)/10;
        int disMod = (Integer.parseInt(Discipline)%1000)/10;

        //Level values
        TextView strLevelValue = (TextView)currentView.findViewById(R.id.strLevel);
        strLevelValue.setText("Level   " +strLevel);

        TextView intLevelValue = (TextView)currentView.findViewById(R.id.intLevel);
        intLevelValue.setText("Level   " +intLevel);

        TextView disLevelValue = (TextView)currentView.findViewById(R.id.disLevel);
        disLevelValue.setText("Level   " +disLevel);

        //progress bars
        ProgressBar strBarr = (ProgressBar)currentView.findViewById(R.id.strBarr);
        strBarr.setProgress(strMod);
        ProgressBar intBarr = (ProgressBar)currentView.findViewById(R.id.intBarr);
        intBarr.setProgress(intMod);
        ProgressBar disBarr = (ProgressBar)currentView.findViewById(R.id.disBarr);
        disBarr.setProgress(disMod);

        //listview stuff
        SQL_Save_Request db = new SQL_Save_Request(getContext());
        //db.addRequest(new Request("HW", "DO IT", "03/21/17", "strength"));
        final ListAdapter stuff = new ListAdapter(getContext(), db.getAllRequests());
        ListView requestList = (ListView) currentView.findViewById(R.id.requestlist);
        requestList.setAdapter(stuff);
        requestList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.edit_delete_dialog);
                dialog.getWindow().addFlags(Window.FEATURE_NO_TITLE);
                dialog.show();
                return false;
            }
        });


        return currentView;
    }
}