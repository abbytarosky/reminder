package classof19.reminder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.PreparedStatement;
import java.util.ArrayList;

/**
 * Created by Abby on 2/23/2017.
 */

public class SQL_Save_Request extends SQLiteOpenHelper{

    private static int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "RequestDB";

    // Books table name
    private static final String TABLE_BOOKS = "requests";

    // Books Table Columns names
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String DUE = "due";
    private static final String DESCR = "description";
    private static final String CATEGORY = "category";
    private static final String COMPLETE = "complete";

    private static final String[] COLUMNS = {ID, NAME,DUE,DESCR, CATEGORY, COMPLETE};

    public SQL_Save_Request(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // SQL statement to create book table
        String CREATE_BOOK_TABLE = "CREATE TABLE requests ( " +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "name TEXT, " +
                "due TEXT, "+
                "description TEXT, " +
                "category TEXT, " +
                "complete INTEGER DEFAULT 0)";

        // create books table
        db.execSQL(CREATE_BOOK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS requests");

        // create fresh books table
        this.onCreate(db);
    }

    public void addRequest(Request rq){
        //for logging
        Log.d("addBook", rq.toString());

        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(NAME, rq.getName()); // get title
        values.put(DUE, rq.getDue_date()); // get author
        values.put(DESCR, rq.getDescription()); // get author
        values.put(CATEGORY, rq.getCategory()); // get author
        values.put(COMPLETE, rq.getComplete());

        // 3. insert
        db.insert(TABLE_BOOKS, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        // 4. close
        db.close();
    }

    public Request getBook(String name){

        // 1. get reference to readable DB
        SQLiteDatabase db = this.getReadableDatabase();

        // 2. build query
        Cursor cursor =
                db.query(TABLE_BOOKS, // a. table
                        COLUMNS, // b. column names
                        " name = ?", // c. selections
                        new String[] { String.valueOf(name) }, // d. selections args
                        null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit

        // 3. if we got results get the first one
        if (cursor != null)
            cursor.moveToFirst();

        // 4. build book object
        Request book = new Request();
        book.setId(cursor.getInt(0));
        book.setName(cursor.getString(1));
        book.setDue_date(cursor.getString(2));
        book.setDescription(cursor.getString(3));
        book.setCategory(cursor.getString(4));
        try {
            book.setComplete(cursor.getInt(5));
        } catch (Exception e) {book.setComplete(0); }

        //log
        Log.d("getBook("+name+")", book.toString());

        // 5. return book
        return book;
    }


    public ArrayList<Request> getAllRequests() {
        ArrayList<Request> requests = new ArrayList<Request>();

        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_BOOKS  + " ORDER BY due ASC";
        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        // 3. go over each row, build book and add it to list
        Request book = null;
        if (cursor.moveToFirst()) {
            do {
                book = new Request();
                book.setId(cursor.getInt(0));
                book.setName(cursor.getString(1));
                book.setDue_date(cursor.getString(2));
                book.setDescription(cursor.getString(3));
                book.setCategory(cursor.getString(4));
                try {
                    book.setComplete(cursor.getInt(5));
                } catch (Exception e) {book.setComplete(0); }

                // Add book to books
                requests.add(book);
            } while (cursor.moveToNext());
        }

        Log.d("getAllBooks()", requests.toString());

        // return books
        return requests;
    }

    public ArrayList<Request> getRequestsForDate(String date)
    {
        ArrayList<Request> requests = new ArrayList<Request>();
        // 1. build the query
        String query = ("SELECT  * FROM " + TABLE_BOOKS  + " WHERE due = \"" + date + "\"");

        Log.e(query,date);
        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        // 3. go over each row, build book and add it to list
        Request book = null;
        if (cursor.moveToFirst()) {
            do {
                book = new Request();
                book.setId(cursor.getInt(0));
                book.setName(cursor.getString(1));
                book.setDue_date(cursor.getString(2));
                book.setDescription(cursor.getString(3));
                book.setCategory(cursor.getString(4));
                try {
                    book.setComplete(cursor.getInt(5));
                } catch (Exception e) {book.setComplete(0); }

                // Add book to books
                requests.add(book);
            } while (cursor.moveToNext());
        }

        Log.d("getAllBooks()", requests.toString());

        // return books
        return requests;
    }

    public ArrayList<String> getAllNames() {
        ArrayList<String> name = new ArrayList<String>();

        // 1. build the query
        String query = "SELECT  * FROM " + TABLE_BOOKS;

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // 3. go over each row, build book and add it to list
        Request book = null;
        if (cursor.moveToFirst()) {
            do {
                book = new Request();
                book.setId(cursor.getInt(0));
                book.setName(cursor.getString(1));
                book.setDue_date(cursor.getString(2));
                book.setDescription(cursor.getString(3));
                book.setCategory(cursor.getString(4));
                try {
                    book.setComplete(cursor.getInt(5));
                } catch (Exception e) {book.setComplete(0); }

                // Add book to books
                name.add(book.getName());
            } while (cursor.moveToNext());
        }

        Log.d("getAllBooks()", name.toString());

        // return books
        return name;
    }

    public int updateRequest(Request rq) {

        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(NAME, rq.getName()); // get title
        values.put(DUE, rq.getDue_date()); // get author
        values.put(DESCR, rq.getDescription()); // get author
        values.put(CATEGORY, rq.getCategory()); // get author
        values.put(COMPLETE, rq.getComplete());

        // 3. updating row
        int i = db.update(TABLE_BOOKS, //table
                values, // column/value
                ID+" = ?", // selections
                new String[] { String.valueOf(rq.getId()) }); //selection args

        // 4. close
        db.close();

        return i;

    }



}
