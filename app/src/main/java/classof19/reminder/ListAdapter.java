package classof19.reminder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Samuel on 4/4/2017.
 */

public class ListAdapter extends ArrayAdapter<Request> {

    public ListAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public ListAdapter(Context context, List<Request> items) {
        super(context,R.layout.listview_singrequest, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.listview_singrequest, null);
        }

        Request p = getItem(position);
/*
        CheckBox completed = (CheckBox) v.findViewById(R.id.completedTask);

        completed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {

            }
        }
        );*/

        TextView name = (TextView) v.findViewById(R.id.name);
        TextView category = (TextView) v.findViewById(R.id.category);
        TextView duedate = (TextView) v.findViewById(R.id.duedate);
        TextView description = (TextView) v.findViewById(R.id.description);

        name.setText(p.getName());
        category.setText(p.getCategory());
        duedate.setText(p.getDue_date());
        description.setText(p.getDescription());







        return v;
    }

}