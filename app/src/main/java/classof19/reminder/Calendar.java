package classof19.reminder;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

public class Calendar extends Fragment {

    View currentView;
    boolean weekshown = true;
    SQL_Save_Request db;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        currentView = inflater.inflate(R.layout.activity_calender, container, false);

        final Button month = (Button)currentView.findViewById(R.id.month);
        final Button week = (Button)currentView.findViewById(R.id.week);
        final ViewSwitcher viewSwitcher = (ViewSwitcher)currentView.findViewById(R.id.week_month);

        final ListView sunday = (ListView)currentView.findViewById(R.id.sunday);
        final ListView monday = (ListView)currentView.findViewById(R.id.monday);
        final ListView tuesday = (ListView)currentView.findViewById(R.id.tuesday);
        final ListView wednesday = (ListView)currentView.findViewById(R.id.wednesday);
        final ListView thursday = (ListView)currentView.findViewById(R.id.thursday);
        final ListView friday = (ListView)currentView.findViewById(R.id.friday);
        final ListView saturday = (ListView)currentView.findViewById(R.id.saturday);

        db = new SQL_Save_Request(getActivity());


        //to get sunday
        final CalendarListAdapter sun_adapter = createAdapter(1);
        sunday.setAdapter(sun_adapter);
        sunday.setOnItemClickListener(CustomOnClick(sun_adapter));

        //to get monday
        CalendarListAdapter mon_adapter = createAdapter(2);
        monday.setAdapter(mon_adapter);
        monday.setOnItemClickListener(CustomOnClick(mon_adapter));
        
        //to get tuesday
        CalendarListAdapter tue_adapter = createAdapter(3);
        tuesday.setAdapter(tue_adapter);
        tuesday.setOnItemClickListener(CustomOnClick(tue_adapter));

        //to get wednesday
        CalendarListAdapter wed_adapter = createAdapter(4);
        wednesday.setAdapter(wed_adapter);
        wednesday.setOnItemClickListener(CustomOnClick(wed_adapter));

        //to get thursday
        CalendarListAdapter thu_adapter = createAdapter(5);
        thursday.setAdapter(thu_adapter);
        thursday.setOnItemClickListener(CustomOnClick(thu_adapter));

        //to get firday
        CalendarListAdapter fri_adapter = createAdapter(6);
        friday.setAdapter(fri_adapter);
        friday.setOnItemClickListener(CustomOnClick(fri_adapter));

        //to get saturday
        CalendarListAdapter sat_adapter = createAdapter(7);
        saturday.setAdapter(sat_adapter);
        saturday.setOnItemClickListener(CustomOnClick(sat_adapter));


        //change back and forth
        month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!weekshown)
                    return;
                month.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                week.setBackgroundColor(getResources().getColor(R.color.colorAccentLight));
                viewSwitcher.showNext();
                weekshown = false;
            }
        });
        week.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(weekshown)
                    return;
                month.setBackgroundColor(getResources().getColor(R.color.colorAccentLight));
                week.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                viewSwitcher.showNext();
                weekshown = true;
            }
        });


        return currentView;
    }

    public String FormatDate(String m)
    {
        if(m.length() == 1)
            m = "0" + m;
        return m;
    }
    
    public CalendarListAdapter createAdapter(int subtractAmount)
    {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.add(java.util.Calendar.DAY_OF_WEEK, -(calendar.get(java.util.Calendar.DAY_OF_WEEK) - subtractAmount));
        String month = (calendar.get(java.util.Calendar.MONTH)+1) + "";
        String day = calendar.get(java.util.Calendar.DATE) + "";
        month = FormatDate(month);
        day = FormatDate(day);
        return new CalendarListAdapter(getActivity(), db.getRequestsForDate(month + "/" + day + "/" + calendar.get(java.util.Calendar.YEAR)));

    }

    public AdapterView.OnItemClickListener CustomOnClick(final CalendarListAdapter c)
    {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Request ra = c.getItem(position);
                ra.setComplete(ra.getComplete() == 0 ? 1 : 0);
                db.updateRequest(ra);

                TextView title = (TextView) view.findViewById(R.id.name);
                if (ra.getComplete() == 1) {
                    title.setPaintFlags(title.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    title.setTextColor(getResources().getColor(R.color.colorAccentLight));
                } else {
                    title.setPaintFlags(Paint.FAKE_BOLD_TEXT_FLAG);
                    title.setTextColor(getResources().getColor(R.color.colorPrimary));
                }

            }
        };
    }

    public void replaceFragment(Fragment fragment) {

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        getFragmentManager().popBackStack();

        fragmentTransaction.commit();
    }
}

