package classof19.reminder;

/**
 * Created by Abby on 2/23/2017.
 */

public class Request {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private int id;
    private String name;
    private String due_date;
    private String description;
    private String category;

    public int getComplete() {
        return complete;
    }

    public void setComplete(int complete) {
        this.complete = complete;
    }

    private int complete;

    public Request() {}

    public Request(String name, String due_date, String description, String category) {
        id = 0;
        this.name = name;
        this.due_date = due_date;
        this.description = description;
        this.category = category;
        this.complete = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDue_date() {
        return due_date;
    }

    public void setDue_date(String due_date) {
        this.due_date = due_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
